<?php
/**
 * @file
 * Contains the TUManagedFileIterator.
 */

/**
 * The TUManagedFileIterator controller.
 * 
 * @param array $files
 * An array of files.
 */
class TUManagedFileIterator extends FilterIterator {

  public function __construct(Iterator $files) {
    parent::__construct($files);
  }

  /**
   * Regex for a path that has been archived. Intended to be passed through
   * sprintf with a datestamp.
   */
  const ARCHIVED_REGEX = '#%s\/[^\/\.]+\.[^\.\/]+$#';

  /**
   * Check that the current file is not already archived.
   *
   * @return boolean
   * TRUE if the current file is NOT archived, otherwise FALSE.
   */
  public function accept() {
    $file = parent::current();
    $archive = format_date($file->timestamp, 'custom', 'Y/m/d');
    $regex = sprintf(self::ARCHIVED_REGEX, $archive);
    return empty(preg_match($regex, $file->uri));
  }

}

