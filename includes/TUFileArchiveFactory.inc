<?php
/**
 * @file
 * Contains the TUFileArchiveFactory.
 */

class TUFileArchiveFactory {

  /**
   * Creates a file iterator for archiving.
   *
   * @return object
   * An instance of TUManagedFileIterator.
   */
  public static function create() {
    $query = db_query('SELECT fid, uri, timestamp FROM file_managed');
    $file_records = new ArrayIterator($query->fetchAllAssoc('fid'));
    $file_iterator = new TUManagedFileIterator($file_records);
    return $file_iterator;
  }

}