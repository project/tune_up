<?php
/**
 * @file
 * Contains the TUFileManager.
 */

/**
 * The TUFileManager controller.
 */
class TUFileManager {

  /**
   * Regex to clean up (remove numeric-only) directory extensions from the 
   * original file path before archiving.
   */
  const PATH_CLEANUP_REGEX = '#((?:\/[0-9]+)+)$#';

  /**
   * Archive a file by moving it to a directory based on its' timestamp.
   * 
   * @param object $file
   * A stdClass file object with at least fid, filename, uri and timestamp.
   */
  public static function archive($file) {
    $dirname = preg_replace(self::PATH_CLEANUP_REGEX, '', drupal_dirname($file->uri));
    $archive_path = "{$dirname}/" . format_date($file->timestamp, 'custom', 'Y/m/d');
    file_prepare_directory($archive_path, FILE_CREATE_DIRECTORY);
    file_move($file, file_stream_wrapper_uri_normalize("{$archive_path}/$file->filename"), FILE_EXISTS_REPLACE);
  }

}

