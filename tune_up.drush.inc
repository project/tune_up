<?php
/**
 * @file
 * Drush commands for the Tune Up module.
 */

/**
 * Implements hook_drush_command().
 */
function tune_up_drush_command() {
  $items = array();

  $items['tu-file-status'] = array(
    'aliases' => array('tufs'),
    'description' => 'Lists the archive status of managed files.'
  );

  $items['tu-file-archive'] = array(
    'aliases' => array('tufa'),
    'description' => 'Archives files.'
  );

  return $items;
}

/**
 * Callback for drush tu-file_status.
 */
function drush_tune_up_tu_file_status() {
  // We are going to use drush_print_table().
  $rows = array();
  $rows[] = array('Archived Files', 'Total Files', 'Percent Archived');
  $file_iterator = TUFileArchiveFactory::create();
  $record_count = $file_iterator->count();
  $archived_count = $record_count - iterator_count($file_iterator);
  $percent_archived = number_format($archived_count/$record_count * 100, 2) . '%';
  $rows[] = array($archived_count, $record_count, $percent_archived);
  drush_print_table($rows, TRUE);
}

/**
 * Callback for drush tu-archive-files.
 */
function drush_tune_up_tu_file_archive() {
  module_load_include('inc', 'tune_up', 'tune_up.batch');
  $batch_definition = tune_up_batch_file_archive();
  batch_set($batch_definition);
  drush_backend_batch_process();
}

