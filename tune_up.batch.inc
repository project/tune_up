<?php
/**
 * @file
 * Batch definitions for the Tune Up module.
 */

/**
 * Defines the batch definition for file archiving.
 *
 * @return array
 * A batch definition suitable to be passed to Drupal's batch_set().
 */
function tune_up_batch_file_archive() {
  $file_iterator = TUFileArchiveFactory::create();
  $batch_definition = array(
    'operations' => array(),
  );
  foreach ($file_iterator as $file) {
    $batch_definition['operations'][] = array('\TUFileManager::archive', array($file));
  }
  return $batch_definition;
}